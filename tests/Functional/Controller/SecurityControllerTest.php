<?php

namespace App\Tests\Functional\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class SecurityControllerTest extends WebTestCase
{
    public function testLoginPageIsUp()
    {
        $client = static::createClient();
        $client->request('GET', '/login');
        $this->assertResponseStatusCodeSame(200);
        $this->assertSelectorExists('input[type="email"]');
        $this->assertSelectorExists('input[type="password"]');
        $this->assertSelectorExists('button[type="submit"]');
        $this->assertSelectorNotExists('.alert.alert-danger');
    }

    public function testWrongLogin()
    {
        $client = static::createClient();
        $client->request('GET', '/login');
        $client->submitForm('Connexion', [
            'email' => 'test@example.com',
            'password' => 'test'
        ]);
        $this->assertResponseRedirects();
        $client->followRedirect();
        $this->assertSelectorExists('.alert.alert-danger');
    }

    public function testLoginSuccessfull()
    {
        $client = static::createClient();
        $client->request('GET', '/login');
        $client->submitForm('Connexion', [
            'email' => 'main@snowtricks.com',
            'password' => 'mainpassword'
        ]);
        $this->assertResponseRedirects('/');
        $client->followRedirect();
        $this->assertSelectorExists('.alert.alert-success');
    }

    public function testLoginWithUnverifiedUser()
    {
        $client = static::createClient();
        $client->request('GET', '/login');
        $client->submitForm('Connexion', [
            'email' => 'disabled@snowtricks.com',
            'password' => 'disabledpassword'
        ]);
        $this->assertResponseRedirects('/');
        $client->followRedirect();
        $this->assertSelectorExists('.alert.alert-success');
    }
}