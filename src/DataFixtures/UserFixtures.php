<?php

namespace App\DataFixtures;

use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class UserFixtures extends Fixture
{
    /**
     * @var UserPasswordEncoderInterface
     */
    private $encoder;

    /**
     * UserFixtures constructor.
     * @param UserPasswordEncoderInterface $encoder
     */
    public function __construct(UserPasswordEncoderInterface $encoder)
    {
        $this->encoder = $encoder;
    }

    /**
     * @inheritDoc
     */
    public function load(ObjectManager $manager)
    {
        $main = new User();
        $main->setEmail('main@snowtricks.com')
            ->setPassword($this->encoder->encodePassword($main, 'mainpassword'))
            ->setIsVerified(true);
        $manager->persist($main);

        $alt = new User();
        $alt->setEmail('alt@snowtricks.com')
            ->setPassword($this->encoder->encodePassword($alt, 'altpassword'))
            ->setIsVerified(true);
        $manager->persist($alt);

        $disabled = new User();
        $disabled->setEmail('disabled@snowtricks.com')
            ->setPassword($this->encoder->encodePassword($disabled, 'disabledpassword'))
            ->setIsVerified(false);
        $manager->persist($disabled);

        $manager->flush();
    }
}